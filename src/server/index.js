const csv = require('csv-parser');
const fs = require('fs');
const { numberOfMatchesPerYear, numberOfMatchesWonPerTeamPerYear, extraRunConcededPerTeamIn2016, top10EconomicalBowlersIn2015 } = require('./ipl');
const matches = [];
const deliveries = [];

fs.createReadStream('/home/gopalpal/Gopal/IPL Directory/src/data/matches.csv')
    .pipe(csv({}))  //pipe it with csv-parser to parse the data as object
    .on('data', (data) => matches.push(data))   //pushung the object data into array to make it array of object   
    .on('end', () => {
        fs.createReadStream('/home/gopalpal/Gopal/IPL Directory/src/data/deliveries.csv') // nesting readstrem for the both CSV files to read and use the data to perform operations 
            .pipe(csv({}))
            .on('data', (data) => deliveries.push(data))
            .on('end', () => {
                const noOfMatchesPerYear = numberOfMatchesPerYear(matches);
                const noOfMatchesWonPerTeamPerYear = numberOfMatchesWonPerTeamPerYear(matches);
                const extraRunbydPerTeamIn2016 = extraRunConcededPerTeamIn2016(matches, deliveries);
                const top10EconomicalBowlersInyear2015 = top10EconomicalBowlersIn2015(matches, deliveries);
                
                // this function dum data resultent data of function into JSON file
                function dumpData(data, file){
                    const finished = (error) => {
                        if (error){
                            console.log(error);
                            return;
                        }
                    }
                    const jsonData = JSON.stringify(data, null, 2);
                    fs.writeFile(file, jsonData, finished);
                }

                dumpData(noOfMatchesPerYear, '/home/gopalpal/Gopal/IPL Directory/src/public/output/noOfMatchesPerYear.json');
                dumpData(noOfMatchesWonPerTeamPerYear, '/home/gopalpal/Gopal/IPL Directory/src/public/output/noOfMatchesWonPerTeamPerYear.json');
                dumpData(extraRunbydPerTeamIn2016, '/home/gopalpal/Gopal/IPL Directory/src/public/output/extraRunbydPerTeamIn2016.json');
                dumpData(top10EconomicalBowlersInyear2015, '/home/gopalpal/Gopal/IPL Directory/src/public/output/top10EconomicalBowlersInyear2015.json');

             });
    });