//Number of matches played per year for all the years in IPL.

function numberOfMatchesPerYear(matches) {
    let result = {};

    for (let i = 0; i < matches.length; i++) {
        if (matches[i].season in result) {
            result[matches[i].season] += 1;
        } else {
            result[matches[i].season] = 1;
        }
    }

    return result;
}

//Number of matches won per team per year in IPL.

function numberOfMatchesWonPerTeamPerYear(matches) {
    let result = {};
    let matchesWonPerTeam = {};

    for (let i = 0; i < matches.length; i++) {
        if (matches[i].season in result) {  
            if (matches[i].winner in matchesWonPerTeam){ 
                matchesWonPerTeam[matches[i].winner] += 1;
            }
            else{
                matchesWonPerTeam[matches[i].winner] = 1;
            }
            result[matches[i].season] = matchesWonPerTeam;
        }
        else {
            matchesWonPerTeam = {};
            result[matches[i].season] = matchesWonPerTeam;
        }
    }

    return result;
}

//Extra runs conceded per team in the year 2016

function extraRunConcededPerTeamIn2016(matches, deliveries){
    let result = {};
    let id = 0;

    for (let i = 0; i < matches.length; i++){
        if (matches[i].season == 2016){
            id = matches[i].id;
        }

        for (let j = 0; j < deliveries.length; j++){
            if (deliveries[j].match_id == id){
                if (deliveries[j].bowling_team in result){
                    result[deliveries[j].bowling_team] += Number(deliveries[j].extra_runs);
                }
                else{
                    result[deliveries[j].bowling_team] = Number(deliveries[j].extra_runs);
                }
            }
        }
    }

    return result;
}

//Top 10 economical bowlers in the year 2015

function top10EconomicalBowlersIn2015(matches, deliveries){
    let result = [];
    let bowlersEconomy = {};
    let id = 0;
    let count = 0;

    for (let i = 0; i < matches.length; i++){
        if (matches[i].season == 2015){
            id = matches[i].id;
        }

        for (let j = 0; j < deliveries.length; j++){
            if (deliveries[j].match_id == id){
                if (deliveries[j].bowler in bowlersEconomy){
                    bowlersEconomy[deliveries[j].bowler] += Number(deliveries[j].total_runs);
                }
                else{
                    bowlersEconomy[deliveries[j].bowler] = Number(deliveries[j].total_runs);
                }
            }
        }
    }

    let sortedBowlersEconomy = Object.entries(bowlersEconomy).sort((a,b) => a[1]-b[1]);
    sortedBowlersEconomy = Object.fromEntries(sortedBowlersEconomy);

    for (let name in sortedBowlersEconomy){
        if (count <= 10){
            result.push(name);
            count += 1;
        }
        else{
            break;
        }
    }

    return result;
}

module.exports = {numberOfMatchesPerYear, numberOfMatchesWonPerTeamPerYear, extraRunConcededPerTeamIn2016, top10EconomicalBowlersIn2015};