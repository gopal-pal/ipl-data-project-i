# IPL Data Project I

This project work on raw data of IPL to calculate the following Statements:

    Number of matches played per year for all the years in IPL.
    Number of matches won per team per year in IPL.
    Extra runs conceded per team in the year 2016
    Top 10 economical bowlers in the year 2015

### Running Locally

Make sure you have [Node.js](https://nodejs.org/en/) installed

```
$ git clone git@gitlab.com:gopal-pal/ipl-data-project-i.git  # or clone your own fork
$ cd IPL-DataProject-I
$ npm install
$ npm start
```

The data is dumped into the output folder as separate JSON files.

See the JSON files in the output folder for the result.

- noOfMatchesPerYear.json file contains the number of matches played in each Year.
- noOfMatchesWonPerTeamPerYear.json file contains the number of matches won by each team in each Year.
- extraRunbydPerTeamIn2016.json file contains the number of extra runs conceded by each team in year 2016.
- top10EconomicalBowlersInyear2015.json file contains top Economical Bowlers who conceded fewer runs in year 2015.